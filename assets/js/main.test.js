﻿jQuery.fn.exists = function(){return this.length>0;}

Number.prototype.padLeft = function (n,str){
    return Array(n-String(this).length+1).join(str||'0')+this;
}

var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

$('select').selectpicker('refresh');

if ($('#fiscalizacion_ofertas').exists()) {
	$('.porcentajes > div:nth(0)').find('.progress_bar span').text('21%');
	$('.porcentajes > div:nth(0) > div > div').css('width', '21%')
	$('.porcentajes > div:nth(1)').find('.progress_bar span').text('14%');
	$('.porcentajes > div:nth(1) > div > div').css('width', '14%')
	$('.porcentajes > div:nth(2)').find('.progress_bar span').text('22%');
	$('.porcentajes > div:nth(2) > div > div').css('width', '22%')
	$('.porcentajes > div:nth(3)').find('.progress_bar span').text('32%');
	$('.porcentajes > div:nth(3) > div > div').css('width', '32%')
	$('.porcentajes > div:nth(4)').find('.progress_bar span').text('23.9%');
	$('.porcentajes > div:nth(4) > div > div').css('width', '23.9%')
	$('.porcentajes > div:nth(5)').find('.progress_bar span').text('29%');
	$('.porcentajes > div:nth(5) > div > div').css('width', '29%')
	$('.porcentajes > div:nth(6)').find('.progress_bar span').text('25%');
	$('.porcentajes > div:nth(6) > div > div').css('width', '25%')
	$('.porcentajes > div:nth(7)').find('.progress_bar span').text('25%');
	$('.porcentajes > div:nth(7) > div > div').css('width', '25%')
	$('.porcentajes > div:nth(8)').find('.progress_bar span').text('22%');
	$('.porcentajes > div:nth(8) > div > div').css('width', '22%')
	$('.porcentajes > div:nth(9)').find('.progress_bar span').text('33%');
	$('.porcentajes > div:nth(9) > div > div').css('width', '33%')
	$('.promedio strong').text('24.2%');
}

//$('.cabecera .fechas p').html('Del 2 al 5 de noviembre');
//$('.cabecera .fechas').css('display', 'inline-block');

var working = false;
var APOYOS_PER_LINE = 6;
var PLATINUM_PER_LINE = 5;
var BLACK_PER_LINE = 4;
var CACHE_ENABLED = true;
var delayTimer;

if (window.innerWidth <= 800) {
	APOYOS_PER_LINE = 2;
	PLATINUM_PER_LINE = 3;
	BLACK_PER_LINE = 2;
}

var window_top = $(window).scrollTop();
var window_bottom = window_top + $(window).height();
var scroll_pos, scroll_elem_width, scroll_width;
var width = $(window).width();

$(window).resize(function(){
	width = $(window).width();
});


function get_window_top(){
	window_top = $(window).scrollTop();
	return window_top;
}

function get_window_bottom(){
	window_bottom = window_top + $(window).height();
	return window_bottom;
}


function close_popup(){
	$('.popup').fadeOut(function(){
		//$('.popup').remove();
	});

	$('body').css('overflow', 'auto');
}



// FUNCIÓN CAMBIO LOGO MEGAOFERTAS

function change_mo_active(elem, direction){
	var parent_btn = $(elem).parents('.slide_content');
	var item_active = $(parent_btn).find('.slide_item.active');

	$(parent_btn).find('.slide_item').removeClass('active');

	if( direction == 'next' ){
		$(item_active).next().addClass('active');
	}else if( direction == 'prev' ){
		$(item_active).prev().addClass('active');
	}
}


function trackBrandImpressions() {
	if ($('.lnkMarca').length) {
		var impressions = [];

		$('.lnkMarca').each(function(index, obj){
			var me = $(obj);

			item = {
				'id': $(this).data('msoc'),
				'name': $(this).data('brand'),
				'price': 1,
				'dimension1': 'Branding',
				'variant': $(this).closest('.variant').data('variant'),
				'brand': $(this).data('brand'),
				'category': $(this).closest('.variant').data('category'),
				'list': $(this).closest('.variant').data('list'),
				'position': $(this).closest('.indexed').index()
			};

			if (item.brand) {
				impressions.push(item);
			}

		});

		dataLayer.push({
			'event': 'productImpression',
			'ecommerce': {
				'currencyCode': 'ARS',
				'impressions': impressions
			}
		});
	}	
}

function trackImpressions() {
	if ($('.mo-item').length) {
		var impressions = [];

		$('.mo-item').each(function(index, obj){
			var me = $(obj).find('.trackPurchase');

			megaoferta = {
				'id': $(me).data('mid'),
				'name': $(this).find('.descripcion').text(),
				'price': $(me).data('precio'),
				'variant': $(me).data('marca'),
				'brand': $(me).data('marca'),
				'list': location.pathname,
				'category': $(me).data('cat'),
				'position': $(me).data('index'),
				'dimension1': $(me).data('type')
			};

			impressions.push(megaoferta);

		});

		dataLayer.push({
			'event': 'productImpression',
			'ecommerce': {
				'currencyCode': 'ARS',
				'impressions': impressions
			}
		});
	}	
}

function trackImpressionsDeferred(data) {
	var impressions = [];

	$($.parseHTML(data)).filter('.mo-item').each(function(index, obj){
			var me = $(obj).find('.trackPurchase');

			megaoferta = {
				'id': $(me).data('mid'),
				'name': $(this).find('.descripcion').text(),
				'price': $(me).data('precio'),
				'variant': $(me).data('marca'),
				'brand': $(me).data('marca'),
				'list': location.pathname,
				'category': $(me).data('cat'),
				'position': $(me).data('index'),
				'dimension1': $(me).data('type')
			};

			impressions.push(megaoferta);

	});

	dataLayer.push({
		'event': 'productImpression',
		'ecommerce': {
			'currencyCode': 'ARS',
			'impressions': impressions
		}
	});
}


function trackNoResults() {
	dataLayer.push({'event':'SinOfertas'});
}


function trackSuscripcion() {
	lead = '/' + context;
	
	dataLayer.push({'event':'Suscripcion', 'Suscripcion_Lead' : lead});
}

// OWL-CAROUSEL
$('.categorias .owl-carousel').owlCarousel({
	autoplay: true,
	autoplayTimeout: 5000,
	margin: 20,
	stagePadding: 30,
	responsive: {
		0: {
			items: 4
		},
		430: {
			items: 5
		},
		500: {
			items: 6
		},
		650: {
			items: 7
		},
		750: {
			items: 8
		},
		870: {
			items: 9
		}
	}
});

var mo_item_carousel = $('.mo-item .owl-carousel');

mo_item_carousel.owlCarousel({
	items: 1,
	loop: true,
	navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
	nav: true
});


$('.productos_hot .owl-carousel').owlCarousel({
	margin: 20,
	nav: true,
	loop: false,
	navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
	responsive: {
		0: {
			items: 2,
			stagePadding: 25
		},
		285: {
			items: 2,
			stagePadding: 30
		},
		300: {
			items: 2,
			stagePadding: 37
		},
		320: {
			items: 2,
			stagePadding: 35
		},
		330: {
			items: 2,
			stagePadding: 35
		},
		350: {
			items: 2,
			stagePadding: 50
		},
		370: {
			items: 2,
			stagePadding: 60
		},
		400: {
			items: 2,
			stagePadding: 70
		},
		430: {
			items: 3,
			stagePadding: 30
		},
		470: {
			items: 3,
			stagePadding: 35
		},
		490:{
			items: 3,
			stagePadding: 40
		},
		635:{
			items: 4,
			stagePadding: 40
		},
		767: {
			items: 5,
			stagePadding: 40
		},
		992: {
			items: 5,
			stagePadding: 40
		}
	}
});

/* ES ACA */
var megaofertas_logos_carousel = $('.megaofertas_head .owl-carousel');

megaofertas_logos_carousel.owlCarousel({
	loop: true,
	center: true,
	autoplay: false,
	nav: true,
	autoWidth: true,
	responsive: {
		0: {
			items: 3
		},
		767: {
			items: 5
		},
		992: {
			items: 7
		}
	},
	navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>']
});


megaofertas_logos_carousel.on('changed.owl.carousel', function(event) {
	$('.owl-item .marca.active').removeClass('active');
	$('.btnMarcaTop.marca_active').removeClass('marca_active');

	setTimeout(function(){
		$('.owl-item.active.center .marca').addClass('active').find('.btnMarcaTop').trigger('click');
	}, 1);
})

/*
$(megaofertas_logos_carousel).find('.owl-nav .owl-next').click(function(){

	var owl_active = $(megaofertas_logos_carousel).find('.owl-item.marca_active');

	$(megaofertas_logos_carousel).find('.owl-item.marca_active').removeClass('marca_active');
	$(megaofertas_logos_carousel).find('.owl-item .marca').removeClass('active');

	$(owl_active).next().addClass('marca_active');
	$(owl_active).next().find('.marca').addClass('active');

});


$(megaofertas_logos_carousel).find('.owl-nav .owl-prev').click(function(){

	var owl_active = $(megaofertas_logos_carousel).find('.owl-item.marca_active');

	$(megaofertas_logos_carousel).find('.owl-item.marca_active').removeClass('marca_active');
	$(megaofertas_logos_carousel).find('.owl-item .marca').removeClass('active');

	$(owl_active).prev().addClass('marca_active');
	$(owl_active).prev().find('.marca').addClass('active');

});



$(megaofertas_logos_carousel).find('.marca').click(function(ev){
	ev.preventDefault();

	$(megaofertas_logos_carousel).find('.marca').removeClass('active');
	$(megaofertas_logos_carousel).find('.owl-item').removeClass('marca_active');
	$(this).addClass('active');
	$(this).parents('.owl-item').addClass('marca_active');
});



var megaofertas_logos_carousel = $('.megaofertas_head .owl-carousel');

megaofertas_logos_carousel.find('.owl-item.active').each( function( index ) {
	$(this).attr( 'data-position', index );
});

megaofertas_logos_carousel.on('click', '.owl-item', function() {
	megaofertas_logos_carousel.trigger('to.owl.carousel', $(this).data( 'position' ) );
});
*/


var megaofertas_carousel;

function initCarouselMegaOfertas() {

	if ($('.mo-item').length > 1) {
		megaofertas_carousel = $('.megaofertas_body .owl-carousel');

		megaofertas_carousel.owlCarousel({
			loop: false,
			margin: 10,
			nav: true,
			navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
			responsive: {
				0: {
					items: 1,
					stagePadding: 40
				},
				300: {
					items: 1,
					stagePadding: 60
				},
				340: {
					items: 1,
					stagePadding: 70
				},
				360: {
					items: 1,
					stagePadding: 85
				},
				390: {
					items: 1,
					stagePadding: 100
				},
				500: {
					items: 2,
					stagePadding: 70
				},
				700: {
					items: 3,
					stagePadding: 35
				},
				992: {
					items: 4,
					stagePadding: 0
				}
			}
		});
	}
	else {
		$('.megaofertas_body .owl-carousel').show();
	}
}


if (!$('body').hasClass('megaofertas_bomba')) {
	if( $('.row .filtro section') ){

		$.each( $('.row .filtro section'), function(i, v){
			
			$(v).find('li').css('display', 'none');
			$(v).find('li').slice(0, 4).css('display', 'block');

			if( $(v).find('li').size() > 4 ){
				$(v).append('<a href="javascript:void(0)">Ver Todos</a>');
			}
		});

	}
}

$('.row .filtro section > a').click(function(){
	$(this).parent('section').find('li').fadeIn();
	$(this).remove();
});



function insertParam(key, value)
{
    key = encodeURI(key); value = encodeURI(value);

    var kvp = document.location.search.substr(1).split('&');

    var i=kvp.length; var x; while(i--) 
    {
        x = kvp[i].split('=');

        if (x[0]==key)
        {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join('&'); 
}


function ordenar_vista(vista){
	localStorage.setItem('vista', vista);

	if(vista == 'list'){
		var btn_change = 'column';
		$('.column').removeClass('column').addClass('list');
	}else if(vista == 'column'){
		var btn_change = 'list';
		$('.list').removeClass('list').addClass('column');
	}

	$('.btn_' + vista).parents('.vista').find('a').removeClass('active');
	$('.btn_' + vista).addClass('active');

	$('.vista_mobile.btn_' + btn_change).removeClass('hide');
	$('.vista_mobile.btn_' + vista).addClass('hide');

	$(mo_item_carousel).trigger('refresh.owl.carousel');
}

$('.vista .btn_list').click(function(){
	ordenar_vista('list');

	return false;
});

$('.vista .btn_column').click(function(){
	ordenar_vista('column');

	return false;
});




$('.filter_bar .vista_mobile.btn_list').click(function(){

	ordenar_vista('list');

	return false;
});

$('.filter_bar .vista_mobile.btn_column').click(function(){
	ordenar_vista('column');

	return false;
});



$('.info_listing .owl-carousel').owlCarousel({
	loop: true,
	nav: true,
	autoplay: true,
	autplayTimeout: 7000,
	navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
	margin: 10,
	responsive: {
		0: {
			items: 2,
			stagePadding: 20
		},
		500: {
			items: 3,
			stagePadding: 20
		},
		767: {
			stagePadding: 0
		},
		992: {
			items: 5,
			stagePadding: 3
		}
	}
});



$('div.counter .owl-carousel').owlCarousel({
	loop: true,
	navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
	responsive: {
		0: {
			items: 2,
			stagePadding: 20
		},
		350: {
			items: 2,
			stagePadding: 30
		},
		380: {
			items: 2,
			stagePadding: 45
		},
		450: {
			items: 3
		},
		625: {
			items: 4
		},
		767: {
			items: 5
		}
	}
});

;


// CONTADOR
var now = new Date();
/*
var deadline = new Date("Oct 30 2017");
var deadline2 = new Date("Nov 02 2017");
var strFecha = "2017/10/30";

if (now.getTime() >= deadline.getTime()) {
	deadline = deadline2;
	strFecha = "2017/11/02";
}
*/

if (typeof boom != 'undefined') {
	total_days = 1;
	strFecha = boom;
}
else {
	deadline = new Date("Oct 29 2018");
	strFecha = "2018/11/10 01:17";
	total_days = (deadline - now) / (1000 * 60 * 60 * 24);
}

$(".countdown").countdown(strFecha, function(event) {
	if (event.type == 'finish') {
		insertParam('boom', 1);
	}

	span = '';
	
	if (total_days > 2) {
		dias_horas = "<strong>%D <span>DÍAS</span> <strong>%H<span>HS</span> :</strong> <strong>%M<span>MIN</span></strong>";
	}
	else {
		dias_horas = "<strong>%I<span>HS</span> :</strong> <strong>%M<span>MIN</span> :</strong> <strong>%S<span>SEG</span></strong>";
	}

	if( $(this).parents('.banner_ofertas_bomba, .banner_top').length ){
		dias_horas = "<span><strong>%M</strong> MIN</span> : <span><strong>%S</strong> SEG</span>";
	}

	if( $(this).parents('.bomba').length ){
		dias_horas = "<span><strong>%M</strong></span>:<span><strong>%S</strong></span>";
	}

	$(this).html( event.strftime(span + dias_horas) );
});




// FUNCIÓN CIERRE DE CARTEL DE "NAVEGADOR VIEJO"

$('.ie8 .close_btn').click(function(){
	$('.ie8').fadeOut(function(){
		$(this).remove();
	});
});




// FUNCIÓN PARA ABRIR POPUPS

$('body').on('click', '.popup .btn_cerrar, .popup .btn_back', function(){
	close_popup();
});

$('body').on('click', '.popup > div', function(ev){
	ev.stopPropagation();
});


function show_popup(clase_popup){
	$('.popup').fadeOut();
	var nombre_popup = '.popup.' + clase_popup;
	$(nombre_popup).fadeIn();
	
	$('body').css('overflow', 'hidden');

	if(clase_popup == 'popup_buscador'){
		$(nombre_popup).find('form input[type=text]').focus();
	}
}






// FUNCIÓN POPUP REPORTAR

function popup_reportar(id, marca, titulo) {

	$.ajax({
		type: 'post',
		url: baseURL + 'ofertas/reportar/' + id,
		data: {
			id: id,
			marca: marca,
			titulo: titulo
		}
	}).success(function(data){
		$('.tooltip').remove();

		$('.popup_reportar').remove();
		$('body').prepend(data);

		$('.popup_reportar').fadeIn();

		$('.popup .btn_cerrar, .popup').click(function(){
			var element = $(this).closest('.popup');
			$.when($(this).closest('.popup').fadeOut()).done(function(){
				element.remove();
			});
			$('body').css('overflow', 'auto');
		});

		$('.popup > div').click(function(ev){
			ev.stopPropagation();
		});

	});
	
}



$(window).load(function(){
	var event;

	if(('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch)){
		event = 'touchstart';
	}else{
		event = 'click';
	}

	$('body').on('click', '.btn_reportar', function(){
		var id = $(this).data('ref');
		var marca = $(this).data('marca');
		var titulo = $(this).parent().parent().parent().find('.descripcion').text();

		if(event == 'click'){
			popup_reportar(id, marca, titulo);
		}
		else if(event == 'touchstart'){
			if( $(this).find('.tooltip').size() != 0 ){
				popup_reportar(id, marca, titulo);
			}
		}
	});

});








// FUNCIÓN TOOLTIP
$('body').on('mouseenter', '*[data-tooltip]', function(){
	var texto = $(this).attr('data-tooltip');

	var elem = $(this);

	$(this).append('<div class="tooltip"><p>' + texto + '</p></div>');
	$(this).addClass('tooltip_content');

	function eliminar_tooltip(){
		elem.find('.tooltip').remove();
		elem.removeClass('tooltip_content');
	}

	setTimeout(eliminar_tooltip, 2000);

});

$('body').on('mouseleave', '*[data-tooltip]', function(){
	$(this).find('.tooltip').remove();
	$(this).removeClass('tooltip_content');
});



if(window.navigator.userAgent.indexOf("MSIE ") == -1){
	$('[title]').mouseover(function(){
		$(this).removeAttr('title');
	});
}






$('.banner_megaofertas_mobile .btn_mo_bg').click(function(){
	$('.registrarme_form').slideDown(function(){
		$('.banner_megaofertas_mobile .btn_mo_bg').fadeOut();
	});
});




$('div.btn_newsletter a').click(function(){
	$('.newsletter.mobile').slideDown(function(){
		$('div.btn_newsletter').fadeOut();
	});
});




$('body').on('change', '#fFiltros input[name=categoria]', function(){
	$('#subcategoria2').addClass('hidden');
	$('#subcategoria2 .options_content').empty();
	$('#atributos').empty().addClass('hidden');

	$.get(baseURL + 'ofertas/filtros/categoria/' + $(this).val(), function(data){
		$('#subcategoria1 .options_content').html(data.categorias_html);
		$('#subcategoria1 .selected_options').text('Seleccionar');
		$('#subcategoria1').removeClass('hidden');
	}, "json");
});

$('body').on('change', '#fFiltros input[name=subcategoria1]', function(){
	$('#subcategoria2').addClass('hidden');
	$('#subcategoria2 .options_content').empty();
	$('#atributos').empty().addClass('hidden');

	$.get(baseURL + 'ofertas/filtros/subcategoria/' + $(this).val(), function(data){
		if (data.categorias_html) {
			$('#subcategoria2 .options_content').html(data.categorias_html);
			$('#subcategoria2 .selected_options').text('Seleccionar');
			$('#subcategoria2').removeClass('hidden');
		}

		if (data.atributos_html) {
			$('#atributos').html(data.atributos_html).removeClass('hidden');
		}
	}, "json");
});

$('body').on('change', '#fFiltros input[name=subcategoria2]', function(){
	$('#atributos').empty().addClass('hidden');

	$.get(baseURL + 'ofertas/filtros/atributos/' + $(this).val(), function(data){
		if (data.atributos_html) {
			$('#atributos').html(data.atributos_html).removeClass('hidden');
		}
	}, "json");
});


function controles_filtro(){

	$('body').on('click', '.select_content button', function(){
		$(this).parents('.option_section').toggleClass('open');
	});

	$('body').on('click', '.select_content .options_content.simple label', function(){
		var text_option = $(this).find('span').html();

		$(this).parents('.option_section').removeClass('open');

		$(this).parents('.option_section').find('.selected_options').html(text_option);
	});

	$('body').on('change', '.select_content .options_content.multiple label', function(){

		var text_option = '';

		var options = $(this).parents('.option_section').find('input:checked + span');

		if($(options).size() != 0){
			$.each($(options), function(i, v){
				text_option += $(v).html() + ', ';
			});

			text_option = text_option.slice(0, -2);
		}else{
			text_option = $(this).parents('.option_section').find('button').attr('data-section');
		}

		$(this).parents('.option_section').find('.selected_options').html(text_option);
	});




	$(document).ready(function(){

		//Track de filtros mobile
		if (window.screen.width < 800) {
			if (filters != undefined) {
				for (var i=0; i<filters.length; i++) {
					if (filters[i].value != '') {
						dataLayer.push({'event':'FiltroMobile_' + filters[i].filter, 'value' : filters[i].value});
					}
				}
			}
		}

		if (no_results) {
			trackNoResults();
		}

		$('.footer_3 .info_content').hide();

		$('.selectpick').selectpicker();

		$.each($('.option_section'), function(i, v){
			var text_option = '';
			
			var options = $(this).find('input:checked + span');

			if($(options).size() != 0){
				$.each($(options), function(i, v){
					text_option += $(v).html() + ', ';
				});
				
				text_option = text_option.slice(0, -2);
			}else{
				text_option = $('.option_section').attr('data-section');
			}


			$(options).parents('.option_section').find('.selected_options').html(text_option);

		});

	});

}


function popup_filtro(){

	$.ajax({
		type: 'post',
		url: baseURL + 'ofertas/filtros',
		data: {
			slug: location.href
		}
	}).success(function(data){
		$('.popup_filtro').remove();
		$('body').prepend(data);

		$('.popup_filtro').fadeIn();

		$('.popup .btn_back').click(function(){
			var element = $(this).closest('.popup');
			$.when($(this).closest('.popup').fadeOut()).done(function(){
				element.remove();
			});
		});
	});
	
}

controles_filtro();




// FUNCIONES ACCIÓN SLIDE DE LOGOS 

function load_detect_scroll(elem){
	scroll_pos = scroll_elem_width = 1;
	scroll_width = 101;

	var btn_prev = $(elem).parents('.slide_content').find('.btn_directions.prev');
	var btn_next = $(elem).parents('.slide_content').find('.btn_directions.next');

	if(scroll_width - scroll_pos <= scroll_elem_width + 30) {
		$(btn_next).fadeOut('fast');
	}else if (scroll_width - scroll_pos >= scroll_width - 30){
		$(btn_prev).fadeOut('fast');
	}else{
		$(btn_prev).fadeIn('fast');
		$(btn_next).fadeIn('fast');
	}
}

function detect_scroll(elem){

	scroll_pos = $(elem).scrollLeft();
   	scroll_elem_width = $(elem).outerWidth();

	scroll_width = $(elem).get(0).scrollWidth;

	var btn_prev = $(elem).parents('.slide_content').find('.btn_directions.prev');
	var btn_next = $(elem).parents('.slide_content').find('.btn_directions.next');

	if(scroll_width - scroll_pos <= scroll_elem_width + 30) {
		$(btn_next).fadeOut('fast');
	}else if (scroll_width - scroll_pos >= scroll_width - 30){
		$(btn_prev).fadeOut('fast');
	}else{
		$(btn_prev).fadeIn('fast');
		$(btn_next).fadeIn('fast');
	}
}



function scroll_to_direction(elem, find, direction, cant){

	if( cant == null || cant == undefined ){
		cant = 1;
	}

	var total_width;
	var pos = $(elem).scrollLeft();

	if( cant != 'free' ){

		var item_width = parseFloat( $(elem).find(find + ':eq(1)').css('width') );

		var item_margin = parseFloat( $(elem).find(find + ':eq(1)').css('margin-right') ) + parseFloat( $(elem).find(find + ':eq(1)').css('margin-left') );

		var item_margin_first_item = parseFloat( $(elem).find(find + ':eq(0)').css('margin-left') );

		total_width = ((item_width + item_margin) * cant);

	}else{
		total_width = 200;
	}

	if( direction == 'prev' ){
		total_scroll = pos - total_width;
	}else if( direction == 'next' ){
		total_scroll = pos + total_width;
	}

	$(elem).animate({
		scrollLeft: total_scroll
	}, 300);


	detect_scroll(elem);
}


setTimeout(function(){
	$('.slide_content .btn_directions').click(function(){
		var slide_element = $(this).parents('.slide_content').find('.slide');

		var cant_slides = ( $(this).attr('data-slide') ) ? $(this).attr('data-slide') : 4 ;

		var direction = ( $(this).hasClass('prev') ) ? 'prev' : 'next';

		scroll_to_direction(slide_element, '.slide_item', direction, cant_slides);
	});

	$('.slide_content .slide').scroll(function(){
		detect_scroll($(this));
	});
}, 1000);








// FUNCIÓN ANIM. FOOTER SEO

$('.footer_3 .mas_info').click(function(){

	window_top = get_window_top();
	window_bottom = get_window_bottom();

	if( $('.info_content').is(':hidden') ){
		$(this).find('a span + span').removeClass('icon-down').addClass('icon-up');

		$('.info_content').slideDown();


		$('html, body').animate({
			scrollTop: window_top + 200
		}, 200);

	}else{
		$('.info_content').slideUp();
		$(this).find('a span + span').removeClass('icon-up').addClass('icon-down');
	}
});



// FUNCIÓN ANIM. STICKY HEADER DESKTOP

function hide_menu(){
	var bottom_menu = $('.menu_desktop').offset().top;
	bottom_menu =  $('.menu_desktop').height() + bottom_menu;

	window_top = get_window_top();

	if( window_top > bottom_menu + 100 ){
		$('.menu_sticky').removeClass('oculto');
	} else{
		$('.menu_sticky').addClass('oculto');
		$('.menu_sticky').removeClass('visible');
		$('.menu_sticky .buscador').fadeOut();
	}
}

hide_menu();

$(window).scroll(function(){
	hide_menu();


	var window_top = $(window).scrollTop();
	var window_bottom = window_top + $(window).height();

	if( $('.productos_content').size() != 0 && $('.filter_bar.static').exists() ){
		var productos_top = $('.productos_content').offset().top;
		var productos_bottom = productos_top + $('.productos_content').height();

		var filter_static_top = $('.filter_bar.static').offset().top;
		var filter_static_bottom = filter_static_top + $('.filter_bar.static').height();

		if(window_top > filter_static_bottom && window_bottom < productos_bottom){
			$('.filter_bar.fixed').fadeIn();
		}else{
			$('.filter_bar.fixed').fadeOut();
		}
	}



	// FUNCIÓN PARA ÍNIDICE "TIENDAS OFICIALES"

	if( $('#tiendas_oficiales .links_indice').size() != 0 ){

			var width = $(window).width();


			if( $(window).width() <= 1199){

				var indice_content_top = $('#tiendas_oficiales .indice_content').offset().top;
				var indice_content_bottom = indice_content_top + $('#tiendas_oficiales .indice_content').height();

				var links_indice_top = $('#tiendas_oficiales .links_indice').offset().top;
				var links_indice_bottom = links_indice_top + $('#tiendas_oficiales .links_indice').height();


				if(window_top + 78 >= indice_content_top){

					$('#tiendas_oficiales .links_indice').addClass('fixed').removeClass('top');

				}else if(window_top + 78 < indice_content_top){

					$('#tiendas_oficiales .links_indice').removeClass('fixed').addClass('top');

				}


				if(window_bottom > indice_content_bottom + 100){
					$('#tiendas_oficiales .links_indice').fadeOut();
				}else if(window_bottom < indice_content_bottom + 100){
					$('#tiendas_oficiales .links_indice').fadeIn();
				}

			}

		$(window).resize(function(){
			width = $(window).width();
		});
	}
});




// FUNCIONES "TIENDAS OFICIALES"
/*
if( $('#tiendas_oficiales').size() != 0 ){

	var width = $(window).width();
	var offset_header = 70;

	$(window).resize(function(){
		width = $(window).width();
	});

	$('#tiendas_oficiales .tiendas .ir_arriba').click(function(){
		var fx = setInterval(function(){
			var window_top = $(window).scrollTop();

			window_top = window_top - 50;

			$(window).scrollTop(window_top);

			if( $(window).scrollTop() <= 0 ){
				clearInterval(fx);
			}
		}, 10);
	});

	$('#tiendas_oficiales .links_indice a').click(function(ev){

		var fx;

		clearInterval(fx);

		if(width >= 1200){
			offset_header = 70;
		}else if(width < 1199){
			offset_header = 110;
		}

		var letra = $(this).attr('href');
		var top_letra = $(this).offset().top;

		var top_elem = $('.tiendas').find(letra);
		top_elem = top_elem.offset().top;


		var fx = setInterval(function(){
			var window_top = $(window).scrollTop();

			if($(window).scrollTop() > top_elem){
				$(window).scrollTop(window_top - 15);

				if( ($(window).scrollTop() + offset_header) <= top_elem ){
					clearInterval(fx);
				}
			}else if($(window).scrollTop() < top_elem){
				$(window).scrollTop(window_top + 15);

				if( ($(window).scrollTop() + offset_header) >= top_elem ){
					clearInterval(fx);
				}
			}
		}, 1);

		ev.preventDefault();
		return false;
	});
}
*/



// FUNCIÓN ACCIÓN BUSCADOR STICKY HEADER
$('body').on('click', '.menu_sticky .btn_search', function(ev){
	if( $('.menu_sticky .buscador').is(':hidden') ){
		$('.menu_sticky .buscador').fadeIn();
		$('.menu_sticky .buscador input').focus();
		$(this).parents('.menu.menu_sticky').addClass('visible');
	}else{
		$('.menu_sticky .buscador').fadeOut();
	}
	

	$(window).click(function(){
		if( !$('.menu_sticky .buscador').is(':hidden') ){
			$('.menu_sticky .buscador').fadeOut();
		}
		$('.menu.menu_sticky').removeClass('visible');
	});


	ev.stopPropagation();
});


$('body').on('click', '.menu_sticky .buscador', function(ev){
	if( $('.menu_sticky .buscador').hasClass('visible') ){
		$('.menu_sticky .buscador').removeClass('visible')
	}

	ev.stopPropagation();
});



$('body').on('click', '.btnInteresa', function(e){
	e.preventDefault();
	show_popup('popup_interesa');
});

$('body').on('click', '.btnCerrarInteresa', function(e){
	e.preventDefault();
	close_popup();
})


// FUNCIÓN ACCIÓN BOTON CATEGORÍAS NEWSLETTER FOOTER

$('.select_mutiple button').click(function(ev){
	width = $(window).width();

	if( width >= 992 ){
		$(this).parent('.select_mutiple').toggleClass('open');

		$(window).click(function(){
			$('.select_mutiple').removeClass('open');
		});

		ev.stopPropagation();
	}else{
		show_popup('popup_categorias');
	}
});

$('.select_mutiple input[type="checkbox"]').change(function(ev){
	multiple_checks( $(this) );
});

$('.select_mutiple').click(function(ev){
	ev.stopPropagation();
});

multiple_checks( $(this) );



function multiple_checks(elem){
	$('.select_mutiple input[type="checkbox"]:checked').parent('label').addClass('activo');

	var chequeados = $('.select_mutiple input[type="checkbox"]:checked').size();

	if(chequeados == 1){
		chequeados = '1 seleccionado';
	}else if(chequeados > 1){
		chequeados = chequeados + ' seleccionados';
	}else{
		if( width >= 992 ){
			chequeados = 'Seleccioná una o más opciones';
		}else{
			chequeados = 'Todas las categorías';
		}
	}

	$('.select_mutiple button .text').html(chequeados);

	if( elem.is(':checked') ){
		elem.parent('label').addClass('activo');
	}else{
		elem.parent('label').removeClass('activo');
	}
}


// BOTÓN "LISTO" POPUP MARCAS

$('.popup.popup_categorias form').submit(function(ev){
	ev.preventDefault();

	close_popup();
});







// FUNCIÓN ACCIÓN STICKY HEADER MOBILE

function hide_menu_mobile(){

	var bottom_menu = $('.header .cabecera').offset().top;
	bottom_menu =  $('.header .cabecera').height() + bottom_menu;

	window_top = get_window_top();

	if( window_top > bottom_menu + 40 ){
		$('.header_mobile').addClass('sticky');
		$('.menu_hs_content').addClass('sticky');

		setTimeout(function(){
			$('.menu_hs_content .tooltip.higlight').fadeOut();
		}, 10000);

		if( $('.notification_button .label').length ){
			setTimeout(function(){
				$('.notification_button .label').fadeOut();
			}, 7900)
		}

	}else{
		$('.header_mobile').removeClass('sticky');
		$('.menu_hs_content').removeClass('sticky');
	}

}

hide_menu_mobile();

$(window).scroll(function(){
	hide_menu_mobile();
});

$('.menu_hs_content .btn_hs_menu').click(function(){
	$(this).find('.tooltip.higlight').fadeOut();;
});


function detect_category_title(){

	if( $('.category_list').length ){

		var category_list_top = $('.category_list').offset().top;
		var category_list_bottom = category_list_top + $('.category_list').height();
		var window_top = $(window).scrollTop();
		var elem_top;

		if( (window_top + 62) < category_list_bottom ){
			$.each( $('.category_list .category_items'), function(i, v){
				elem_top = $(v).offset().top;

				if( window_top > elem_top ){
					$(v).find('.title_head.sticky').fadeIn('fast', function(){
						$(this).addClass('active');
					});
				}else{
					$(v).find('.title_head.sticky').fadeOut('fast', function(){
						$(this).removeClass('active');
					});
				}
			});
		}else{
			$('.title_head.sticky').fadeOut('fast', function(){
				$(this).removeClass('active');
			})
		}

	}

}


$(window).scroll(function(){
	detect_category_title();
});


$('#fInteresa').on('submit', function(e){

	$('.msg.error').hide();
	target = baseURL + 'suscripcion';

	$('#fInteresa input[type=submit]').attr('disabled', 'disabled').val('ENVIANDO...');
	$.post(target, $('#fInteresa').serialize(), function(response){
		if (response.status == 'ok') {
			trackSuscripcion('Interesa');

			$('#fInteresa').parent().find('h2').hide();
			$('#fInteresa').empty().append('<div class="msg thanks"><h2>¡Gracias por suscribirte!</h2><p>Muy pronto recibirás novedades.</p><a href="#" class="btnCerrarInteresa">Cerrar</a></div>');
			$('.popup_interesa .content').height('190px');
		}
		else {
			$('#fInteresa input[type=submit]').removeAttr('disabled').val('Enviar');
			$('#fInteresa .msg.error').text(response.error).show();
		}
	}, "json");

	return false;
});

// FUNCIÓN MOSTRAR "PASOS" DE NEWSLETTER MOBILE

function validation_newsletter_mobile(elem){

	$('.msg.error').hide();
	target = baseURL + 'suscripcion';

	$('#fSuscripcion2 input[type=submit]').attr('disabled', 'disabled').val('Por favor esperá...');
	$.post(target, $('#fSuscripcion2').serialize(), function(response){
		if (response.status == 'ok') {
			trackSuscripcion('Suscripcion');

			$('#fSuscripcion2').parent().find('h2').hide();
			$('#fSuscripcion2').empty().append('<div class="msg thanks">¡Gracias por suscribirte! Muy pronto recibirás novedades.</div>');
		}
		else {
			$('#fSuscripcion2 input[type=submit]').removeAttr('disabled').val('Enviar');
			$('.msg.error').text(response.error).show();
		}
	}, "json");

}


$('.newsletter.mobile form').submit(function(ev){

	validation_newsletter_mobile();

	ev.preventDefault();
});


$('#fSeleccion input[type=checkbox]').click(function(){
	$('.int_' + $(this).val()).prop('checked', $(this).is(':checked'));
	var cats = 0;
	$('#fSuscripcion2 input[type=checkbox]').each(function(idx, obj){
		if ($(obj).is(':checked')) {
			cats += 1;
		}
	});
	$('.butSelect span.text').text(cats + ' seleccionada/s');
});

$('#btnSeleccionarCats').click(function(e){
	$('.popup_categorias').fadeOut();
});

$('#fSeleccion').on('submit', function(e){
	e.preventDefault();

	return false;
})

// ANIMACIÓN BARRA DE PROGRESO EN "FISCALIZACIÓN DE OFERTAS"

flag_active = false;

if( $('#fiscalizacion_ofertas').size() != 0 ){

	var window_top = window_top = $(window).scrollTop();
	var elem_top = elem_top = $('#fiscalizacion_ofertas .banner').offset().top;

	$('.porcentajes .progress_bar').css('overflow', 'hidden');
	$('.porcentajes .progress_bar span').css('color', '#ffffff');

	$(window).scroll(function(){
		window_top = $(window).scrollTop();
		elem_top = $('.banner').offset().top;

		anim_porcentajes();
	});

	anim_porcentajes();
	


	function anim_porcentajes(){

		if((window_top >= elem_top - 150) && flag_active == false){

			$('.porcentajes .progress_bar span').each(function(){

				var valor = $(this).html();

				$(this).parent('div').css('width', valor);


				flag_active = true;

				$(this).prop('Counter', 0).animate({
					Counter: $(this).text()
				}, {
					duration: 3000,
					easing: 'swing',
					step: function (now){
						$(this).text(Math.ceil(now) + '%');
					}
				});
			});
		}
	}
}






if( $('#interna_tienda').size() != 0 ){

	var descripcion = $('#interna_tienda .detalles > p').html();

	var descripcion_corta = $('#interna_tienda .detalles > p').html().split(/\s+/).slice(0, 10).join(" ");

	if(descripcion.split(/\s+/).length > 10){
		$('#interna_tienda .detalles > p').html(descripcion_corta + '... <a href="javascript:void(0)" onclick="ver_desc_completa()">Ver más</a>');
	}


	function ver_desc_corta(){
		$('#interna_tienda .detalles > p').html(descripcion_corta + ' <a href="javascript:void(0)" onclick="ver_desc_completa()">Ver más</a>');
	};

	function ver_desc_completa(){
		$('#interna_tienda .detalles > p').html(descripcion + ' <a href="javascript:void(0)" onclick="ver_desc_corta()">Ver menos</a>');
	};
}




// FUNCIÓN PARA MOSTRAR RESULTADOS EN BUSCADOR

$('.buscador form input[type=text]').on('blur focus input change', function(ev){

	var results_box = $(this).parents('.buscador').find('.resultados_busqueda');

	var form_content = $(this).parents('.buscador');

	if( $(this).val().length > 0 ){

		if( $(this).parents('.menu.menu_sticky').length ){
			$(this).parents('.menu.menu_sticky').addClass('visible');
			$(results_box).addClass('visible');
		}else{
			$(results_box).addClass('visible');
			$(form_content).addClass('visible');
		}

		// ToDo: AJAX de resultados
	}else{
		$(results_box).removeClass('visible');
		$(form_content).removeClass('visible');
	}

	$(window).click(function(){
		$(results_box).removeClass('visible');
		$(form_content).removeClass('visible');
		$('.menu.menu_sticky').removeClass('visible');
	});

	$(this).click(function(event){
		event.stopPropagation();
	});


	ev.stopPropagation();
});



(function select_links(){

	var results_box, results_links;

	$('body').on('keydown', function(ev){

		if( $('.buscador .resultados_busqueda.visible').length ){

			//if( results_box === undefined ){
			results_box = $('.buscador .resultados_busqueda.visible');
			//}

			//if( results_links === undefined ){
			results_links = $(results_box).find('a');
			//}

			// TECLA ABAJO
			if(ev.keyCode == 40){
				if( indexBuscador != results_links.length ){
					indexBuscador = indexBuscador + 1;
				}

				$(results_box).find(results_links).eq(indexBuscador).focus();

				ev.preventDefault();
			}


			// TECLA ARRIBA
			if(ev.keyCode == 38){
				if( indexBuscador != 0 ){
					indexBuscador = indexBuscador - 1;
				}

				$(results_box).find(results_links).eq(indexBuscador).focus();

				ev.preventDefault();
			}

		}
	});

})();





// PARA AGREGAR MAS PRODUCTOS
// CARGAR 10 SKU

$('.btn_agregar_productos').click(function(e){
	e.preventDefault();
	var me = $(this);
	var offset = parseInt($(this).data('offset'));
	var size = parseInt($(this).data('size'));
	
	$.get($(this).attr('href') + '&shh=1&next=' + offset, function(data){
		if (data == '') {
			$(me).hide();
		}
		else {
			var $items = $(data);
			$('.productos_content.grid').append($items).isotope('appended', $items);

			$(me).data('offset', offset + size);

			trackImpressionsDeferred(data);
		}
	});

	return false;
});


$('.share_facebook').click(function(e){
	FB.ui({
		method: "share",
		href:location.href
	}, function(response){});
});

$('.category_list .category_items a.btn_hide_brand').click(function(){
	var parent = $(this).closest('.category_items');
	var elem = $(parent).find('.marcas_real_content');

	if( $(elem).is(':hidden') ){
		$(elem).slideDown();
		$(parent).addClass('open');
		$(this).html('Ocultar');
	}else{
		$(elem).slideUp();
		$(parent).removeClass('open');
		$(this).html('Mostrar');
	}
});

$(document).ready(function(){

	//Notificaciones Push
	$(document).on('click', '.btnPush', function(e){
		e.preventDefault();
		console.log('clicked');
		_pe.subscribe({}.pe,function(res){
		    console.log(res);

		    localStorage.push_status = res.statuscode;

		    if(res.statuscode==1)
		    {
		      localStorage.push_subscriber_hash = res.subscriber_hash;
		    }
		});
	});

	if (document.cookie.indexOf('PushSubscriberStatus') == -1) {
		$('.btnPush').show();
	}

	if (iOS) {
		$('.notification_button').hide();
	}

	$('.btnVerBlackHome').click(function(e){
		e.preventDefault();
		$('.popup_marcas_black .cat').text($(this).data('ref'));
		var marcasBlack = $.parseJSON(localStorage.cm18_home)[$(this).data('cat')];

		$('.popup_marcas_black .lista_megaofertas_top').empty();
		for (var i=0; i<marcasBlack.length; i++) {
			var m = marcasBlack[i];
			var obj = '<div class="marca"><div><a class="img_marca" href="#"><img class="img-responsive center-block Sponsor_Lead lnkMarca m' + m.id + '" src="http://cdna.cybermonday.com.ar/uploads/marcas/' + m.id + '/' + m.logo + '" alt="cyber monday 2018 ' + m.marca + '" data-link="' + m.link_home + '" data-mid="' + m.id + '" data-sponsor="Top" data-slug="' + m.slug + '" /><div><span>VER OFERTAS</span></div></a></div><a href="#" class="titulo_marca">' + m.marca + '</a></div>';
			$('.popup_marcas_black .lista_megaofertas_top').append(obj);
		}

		$('.popup_marcas_black .btnLinkCategoria').attr('href', '/' + $(this).data('slug'));

		show_popup('popup_marcas_black');
	});

	// Click en comprar
	$('body').on('click', '.trackPurchase', function(e){
		e.preventDefault();

		var link = $(this).data('link');

		var list = categorias.slice(0, 2);
		if ($(this).data('cat') != '') list.push($(this).data('cat'));

		var name = $(this).closest('article').find('.descripcion').text();
		if (!name) {
			name = $(this).closest('.mo-item').find('.descripcion').text();
		}

		var megaoferta = {
			'id': $(this).data('mid'),
			'name': name,
			'price': $(this).data('precio'),
			'variant': $(this).data('marca'),
			'brand': $(this).data('marca'),
			'category': $(this).data('cat'),
			'position': $(this).data('index'),
			'dimension1': $(this).data('type')
		};

		dataLayer.push({'event':'productClick', 'ecommerce' : {
			'click': {
				'actionField': {'list': location.pathname},
				'products': [megaoferta]
			}
		}});

		window.open(link);
	});

	//Track impresiones de ofertas
	trackImpressions();
	setTimeout(function(){
		trackBrandImpressions();
	}, 3000);

	$('body').on('click', '.btnMarcaTop', function(e){
		e.preventDefault();

		//CORREGIR
		megaofertas_logos_carousel.trigger('to.owl.carousel', [$(this).data('index')]);

		$('#megaofertas-loading').show();
		$('#megaofertas-top').empty();
		$.get(baseURL + 'home/ofertas_top/' + $(this).data('mid'), function(data){
			$('#megaofertas-top').html(data);
			$('#megaofertas-loading').hide();
			initCarouselMegaOfertas();

			trackImpressions();
		});
	});

	if ($('#cat-ofs').exists()) {
		initCarouselMegaOfertas();
	}

	//Precargar primer marca top
	if ($('.megaofertas.carousel').exists()) {
		//initCarouselMegaOfertas();

		$('.owl-item.active .marca.active .btnMarcaTop').trigger('click');
	}
	

	$('.check.filtrolink').click(function(e){
		e.preventDefault();		
		location.href = $(this).data('slug');
	});

	$('.sort').change(function(e){
		location.href = $(this).val();
		e.stopPropagation();
	});

	if ($('.vista').exists()) {
		if (localStorage.getItem('vista')) {
			$('.btn_' + localStorage.getItem('vista')).trigger('click');
		}
	}

	$('.share-email').attr('href', 'mailto:?subject=' + encodeURI('Ofertas Exclusivas Cyber Monday') + '&body=' + encodeURI('Apurate antes de que se terminen!') + ' ' + location.href);

	$('.share-whatsapp').click(function(e){
		e.preventDefault();
		msg = 'Mirá qué buenas ofertas hay en Cyber Monday ¡Aprovechalas antes de que se terminen!';
		location.href = 'whatsapp://send?text=' + encodeURIComponent(msg + ' ' + location.href);
	});

	$('.share-messenger').click(function(e){
		e.preventDefault();
		location.href = 'fb-messenger://share?link=' + encodeURIComponent(location.href);
	});

	$('.share-facebook').click(function(e){
		e.preventDefault();

		FB.ui({
		  method: 'share',
		  href: location.href,
		}, function(response){});
	});

	$('body').on('submit', '#fReportar', function(e){
		e.preventDefault();

		$.post(baseURL + 'ofertas/reportar_send', $('#fReportar').serialize(), function(response){
			if (response.success) {
				$('#fReportar').fadeOut(function(){
					$('#reportar-gracias').fadeIn();
					$('.popup_reportar').addClass('gracias');
				});
			}
			else {
				$('#fReportar #reportar-error').text(response.error);
				$('#fReportar .error').show();
			}
		}, "json");

		return false;
	});

	if ($('#txtVerPerfil').exists()) {
		$('#txtVerPerfil').text('VER PERFIL');
	}

	$('#mtop').owlCarousel({
		loop: true,
		nav: true,
		autoplay: true,
		autplayTimeout: 7000,
		navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
		margin: 10,
		responsive: {
			0: {
				items: 2,
				stagePadding: 20
			},
			500: {
				items: 3,
				stagePadding: 20
			},
			767: {
				stagePadding: 0
			},
			992: {
				items: 5,
				stagePadding: 3
			}
		}
	});

	// Click en sponsor
	$('body').on('click', '.lnkMarca', function(e){
		e.preventDefault();
		var marca = 'm' + $(this).data('mid');

		link = $(this).data('link');		
	
		if ($(this).hasClass('principal') && window.innerWidth <= 800) {
			link = $(this).data('linkm');
		}

		var item = {
			'id': $(this).data('msoc'),
			'name': $(this).data('brand'),
			'price': 1,
			'dimension1': 'Branding',
			'variant': $(this).closest('.variant').data('variant'),
			'brand': $(this).data('brand'),
			'category': $(this).closest('.variant').data('category'),
			'position': $(this).closest('.indexed').index()
		};
		
		r = dataLayer.push({'event':'productClick', 'ecommerce' : {
			'click': {
				'actionField': {'list': $(this).closest('.variant').data('list')},
				'products': [item]
			}
		}});

		window.open(link);
	});

	$('#fSuscripcion1').on('submit', function(e){
		e.preventDefault();
		$.post(baseURL + 'suscripcion', $(this).serialize(), function(response){
			if (response.status == 'ok') {
				$('#fSuscripcion1').empty().append('<p class="gracias">¡Gracias por suscribirte! Muy pronto recibirás novedades.</p>');

				trackSuscripcion('Suscripcion');
			}
			else {
				alert(response.errors[0].error);
			}
		}, "json");

		return false;
	});

	$('#fAdelantate').on('submit', function(e){
		e.preventDefault();
		$.post(baseURL + 'suscripcion', $(this).serialize(), function(response){
			if (response.status == 'ok') {
				$('#fAdelantate').parent().find('p').remove();
				$('#fAdelantate').empty().append('<p class="gracias">¡Gracias por suscribirte! Muy pronto recibirás novedades.</p>');

				trackSuscripcion('Adelantate');
			}
			else {
				alert(response.errors[0].error);
			}
		}, "json");

		return false;
	});

	$('#btnRegistrarme').click(function(e){
		e.preventDefault();
		$.post(baseURL + 'suscripcion', $('#fRegistrarme').serialize(), function(response){
			if (response.status == 'ok') {
				document.getElementById('fRegistrarme').reset();
				close_popup();

				trackSuscripcion('Suscripcion');
			}
			else {
				$('#txt-error').text(response.error);
				$('.msg.error').show();
			}
		}, "json");

		return false;
	});

	$('.chkAll').click(function(){
		$('#fRegistrarme input.c').prop('checked', $(this).is(':checked'));
	});

	$('.selSubmit').on('change', function(){
		$(this).closest('form').submit();
	});

	$('.chkSubmit').on('click', function(){
		$(this).closest('form').submit();
	});

	// Helper para indicar que una marca fue fiscalizada
	Handlebars.registerHelper('fiscalizar', function(options){
		return (options.fn(this) == '1' ? 'on' : 'off');
	});

	// Helper para comparar elementos
	Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
	    if (arguments.length < 3)
	        throw new Error("Handlebars Helper equal needs 2 parameters");
	    if( lvalue!=rvalue ) {
	        return options.inverse(this);
	    } else {
	        return options.fn(this);
	    }
	});

	// Helper para setear el link correspondiente a la marca
	Handlebars.registerHelper('linkear', function(link, link_mobile){
		if (window.innerWidth <= 800) {
			return link_mobile ? link_mobile : link;
		}
		else {
			return link;
		}
	});

	// Completar espacio de logos 
	if ($('#sponsors').exists()) {
		if (current == 'home') {
			endpoint = baseURL + 'alfred/home';
			key = "cm18_home";
		}

		buildSponsors('sponsors-template', key, endpoint, 'sponsors', mis_marcas);

		setTimeout(function(){
			$('.bot-home').addClass('shown');	
		}, 500);
	}

	// Completar espacio de apoyos
	if ($('#sec-apoyos').exists()) {
		build('apoyos-template', 'cm18_apoyos', baseURL + 'alfred/apoyos', 'sec-apoyos', mis_apoyos, APOYOS_PER_LINE);

		setTimeout(function(){
			$('.bot-apoyos').addClass('shown');
		}, 500);
	}

	// Completar espacio de logos black
	if ($('#black').exists()) {
		endpoint = baseURL + 'alfred/categorias/' + categoria + '/black';
		key = "cm18_" + categoria + "_black_1";
		build('marcas-black-template', key, endpoint, 'black', mis_marcas_black, BLACK_PER_LINE);

		setTimeout(function(){
			$('.bot-black').addClass('shown');
		}, 500);
	}

	// Completar espacio de logos platino
	if ($('#platinum').exists()) {
		endpoint = baseURL + 'alfred/categorias/' + categoria + '/platinum';
		key = "cm18_" + categoria + "_platinum_1";
		build('marcas-platinum-template', key, endpoint, 'platinum', mis_marcas_platinum, PLATINUM_PER_LINE);

		setTimeout(function(){
			$('.bot-platinum').addClass('shown');
		}, 500);
	}


	Vue.component('buscador-ofertas', {
		template: "#" + (window.innerWidth < 768 ? "buscador-mobile" : "buscador-desktop"),
		data: function(){
			return { 
				keywords: '',
				show: false,
				show_marcas: false,
				show_productos: false,
				results: {
	      			marcas: [],
	      			productos: []
	    		}
			}
		},
	  	methods: {
		  	suggest: function (value) {
		  		if (value.length > 1) {
			  		var me = this;
			  		indexBuscador = -1;
			  		doSearch(value, me);
			  		$('.buscador').addClass('visible');

					$(window).click(function(){
						$('.buscador').removeClass('visible');
					});	  		
			  	}
			  	else {
			  		$('.buscador').removeClass('visible');

			  		this.results.marcas = [];
		        	this.results.productos = [];
		        	this.show = false;
			      	this.show_marcas = false;
			      	this.show_productos = false;
			  	}
		  	},
		  	clear: function() {
		      	this.keywords = '';
		  		this.results.marcas = [];
		      	this.results.productos = [];
		      	this.show = false;
		      	this.show_marcas = false;
		      	this.show_productos = false;
		  		indexBuscador = -1;
		  	}
	  	}
	});

	var vm1 = new Vue({
	 	el: '#buscador',
	  	data: {
	  		views: ['buscador-ofertas', 'buscador-ofertas-mobile']
	  	},
		methods: {
	    	addComponent: function (component) {
	    		this.views.push(component);
	  		}
	  	}  
	});

	var vm2 = new Vue({
	 	el: '#navi',
	  	data: {
	  		views: ['buscador-ofertas', 'buscador-ofertas-mobile']
	  	},
		methods: {
	    	addComponent: function (component) {
	    		this.views.push(component);
	  		}
	  	}  
	});

	var vm2 = new Vue({
	 	el: '#busc',
	  	data: {
	  		views: ['buscador-ofertas', 'buscador-ofertas-mobile']
	  	},
		methods: {
	    	addComponent: function (component) {
	    		this.views.push(component);
	  		}
	  	}  
	});

	function doSearch(value, me) {
	  clearTimeout(delayTimer);
	  delayTimer = setTimeout(function() {
	    value = value.toLowerCase();
	    value = value.replace('á', 'a');
	    value = value.replace('é', 'e');
	    value = value.replace('í', 'i');
	    value = value.replace('ó', 'o');
	    value = value.replace('ú', 'u');
	    value = value.replace('ñ', 'n');

	    $.ajax({
	      url: API_ENDPOINT + '/suggest?q=' + value,
	      crossDomain: true,
	      dataType: 'jsonp'
	    }).done(function(data){

	      me.results.marcas = data.marcas;
	      if (mv != '0') {
	      	me.results.productos = data.ofertas;
	      }

	      me.show = me.results.marcas.length || me.results.productos.length;
	      me.show_marcas = me.results.marcas.length > 0 ? true : false;
	      me.show_productos = me.results.productos.length > 0 ? true : false;

	    });
	  }, 200);
	}

});


function build(template, key, endpoint, target, items, items_per_line) {
	var source   = $("#" + template).html();
	var template = Handlebars.compile(source);
	var html = '';

	if (items == undefined || !CACHE_ENABLED) {
		$.get(endpoint, function(results){
			localStorage.setItem(key, results);

			html = template(JSON.parse(results));
			$('#' + target).html(html);

			ready(target, items_per_line);
		});	
	}
	else {
		html = template(JSON.parse(items));
		$('#' + target).html(html);

		ready(target, items_per_line);
	}	
}

function buildSponsors(template, key, endpoint, target, items) {
	var source   = $("#" + template).html();
	var template = Handlebars.compile(source);
	var html = '';

	if (items == undefined || !CACHE_ENABLED) {
		$.get(endpoint, function(results){
			localStorage.setItem(key, results);

			cats = JSON.parse(results);

			block = $('#sponsors-electro .block').clone();
			$('#sponsors-electro').html( template({ marcas: cats.electro }) ); 
			$('#sponsors-electro .marcas_real_content').append(block);

			block = $('#sponsors-indumentaria .block').clone();
			$('#sponsors-indumentaria').html( template({ marcas: cats.indumentaria }) ); 
			$('#sponsors-indumentaria .marcas_real_content').append(block);

			block = $('#sponsors-viajes .block').clone();
			$('#sponsors-viajes').html( template({ marcas: cats.viajes }) ); 
			$('#sponsors-viajes .marcas_real_content').append(block);

			block = $('#sponsors-muebles .block').clone();
			$('#sponsors-muebles').html( template({ marcas: cats.muebles }) ); 
			$('#sponsors-muebles .marcas_real_content').append(block);

			block = $('#sponsors-deportes .block').clone();
			$('#sponsors-deportes').html( template({ marcas: cats.deportes }) ); 
			$('#sponsors-deportes .marcas_real_content').append(block);

			block = $('#sponsors-bebes .block').clone();
			$('#sponsors-bebes').html( template({ marcas: cats.bebes }) ); 
			$('#sponsors-bebes .marcas_real_content').append(block);

			block = $('#sponsors-cosmetica .block').clone();
			$('#sponsors-cosmetica').html( template({ marcas: cats.cosmetica }) ); 
			$('#sponsors-cosmetica .marcas_real_content').append(block);

			block = $('#sponsors-automotriz .block').clone();
			$('#sponsors-automotriz').html( template({ marcas: cats.automotriz }) ); 
			$('#sponsors-automotriz .marcas_real_content').append(block);

			block = $('#sponsors-alimentos .block').clone();
			$('#sponsors-alimentos').html( template({ marcas: cats.alimentos }) ); 
			$('#sponsors-alimentos .marcas_real_content').append(block);

			block = $('#sponsors-varios .block').clone();
			$('#sponsors-varios').html( template({ marcas: cats.varios }) ); 
			$('#sponsors-varios .marcas_real_content').append(block);

			block = $('#sponsors-servicios .block').clone();
			$('#sponsors-servicios').html( template({ marcas: cats.servicios }) ); 
			$('#sponsors-servicios .marcas_real_content').append(block);

			ready(target, 1);
		});	
	}
	else {
			cats = JSON.parse(items);

			block = $('#sponsors-electro .block').clone();
			$('#sponsors-electro').html( template({ marcas: cats.electro }) ); 
			$('#sponsors-electro .marcas_real_content').append(block);

			block = $('#sponsors-indumentaria .block').clone();
			$('#sponsors-indumentaria').html( template({ marcas: cats.indumentaria }) ); 
			$('#sponsors-indumentaria .marcas_real_content').append(block);

			block = $('#sponsors-viajes .block').clone();
			$('#sponsors-viajes').html( template({ marcas: cats.viajes }) ); 
			$('#sponsors-viajes .marcas_real_content').append(block);

			block = $('#sponsors-muebles .block').clone();
			$('#sponsors-muebles').html( template({ marcas: cats.muebles }) ); 
			$('#sponsors-muebles .marcas_real_content').append(block);

			block = $('#sponsors-deportes .block').clone();
			$('#sponsors-deportes').html( template({ marcas: cats.deportes }) ); 
			$('#sponsors-deportes .marcas_real_content').append(block);

			block = $('#sponsors-bebes .block').clone();
			$('#sponsors-bebes').html( template({ marcas: cats.bebes }) ); 
			$('#sponsors-bebes .marcas_real_content').append(block);

			block = $('#sponsors-cosmetica .block').clone();
			$('#sponsors-cosmetica').html( template({ marcas: cats.cosmetica }) ); 
			$('#sponsors-cosmetica .marcas_real_content').append(block);

			block = $('#sponsors-automotriz .block').clone();
			$('#sponsors-automotriz').html( template({ marcas: cats.automotriz }) ); 
			$('#sponsors-automotriz .marcas_real_content').append(block);

			block = $('#sponsors-alimentos .block').clone();
			$('#sponsors-alimentos').html( template({ marcas: cats.alimentos }) ); 
			$('#sponsors-alimentos .marcas_real_content').append(block);

			block = $('#sponsors-varios .block').clone();
			$('#sponsors-varios').html( template({ marcas: cats.varios }) ); 
			$('#sponsors-varios .marcas_real_content').append(block);

			block = $('#sponsors-servicios .block').clone();
			$('#sponsors-servicios').html( template({ marcas: cats.servicios }) ); 
			$('#sponsors-servicios .marcas_real_content').append(block);

			ready(target, 1);
	}	
}

function ready(target, items_per_line) {
	$.each($('.Sponsor_Lead'), function(index, item){
		if (fiscalizadas.indexOf($(item).data('mid')) > -1) {
			$(item).parent().parent().find('.fiscalizada').addClass('on');
		}
	});
	
	if (target != '') {
		$.each($('#' + target + ' .Sponsor_Lead'), function(index, item){
			col = (parseInt(index % items_per_line) + 1).padLeft(2);
			row = (parseInt(index / items_per_line) + 1).padLeft(2);
			$(item).attr('data-col', col);
			$(item).attr('data-row', row);
		});	
	}
}

function initSponsorsCarousel(id) {
	$('#sponsors-' + id).owlCarousel({
		stagePadding: 30,
		autoplayTimeout: 7000,
		items: 4,
		nav: true,
		navText: ['<span class="icon-left"></span>', '<span class="icon-right"></span>'],
		responsive: {
			// min-width:
			0: {
				items: 2,
				margin: 7,
				slideBy: 2
			},
			500: {
				items: 3,
				margin: 7,
				slideBy: 3
			},
			992: {
				items: 4,
				margin: 15,
				slideBy: 4
			}
		},
	});
}


if( $('.notification_tooltip').length ){


	setInterval(function(){
		var visible = $('.notification_button .label_text.visible');

		$('.notification_button .label_text').fadeOut(function(){
			$(this).removeClass('visible');
		});

		$(visible).next('span').fadeIn(function(){
			$(this).addClass('visible');
		});

		if( $(visible).next('span').index() == -1 ){
			$('.notification_button .label_text:eq(0)').fadeIn(function(){
				$(this).addClass('visible');
			});
		}
	}, 2000);


	setInterval(function(){
		var visible = $('.notification_content .label_text.visible');

		$('.notification_content .label_text').fadeOut(function(){
			$(this).removeClass('visible');
		});

		$(visible).next('span').fadeIn(function(){
			$(this).addClass('visible');
		});

		if( $(visible).next('span').index() == -1 ){
			$('.notification_content .label_text:eq(0)').fadeIn(function(){
				$(this).addClass('visible');
			});
		}
	}, 2000);

}
