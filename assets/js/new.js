// FUNCIÓN PARA ÍNIDICE "TIENDAS OFICIALES" - DESKTOP STICKY, NAVIGATION Y HIGHLIGHTS

var $navigationLinks = $('.letter-index_letter a');
var $sections = $($(".anchor-link").get().reverse());

var sectionIdTonavigationLink = {};
$sections.each(function() {
    var id = $(this).attr('id');
    sectionIdTonavigationLink[id] = $('.letter-index_letter a[href=\\#' + id + ']');
});


$navigationLinks.on('click', function(e){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top - 100;
  e.preventDefault();
  $('html, body').stop().animate({
      scrollTop: offsetTop
  }, 1000,  function(){
  });
});

function highlightNavigation() {

  var scrollPosition = $(window).scrollTop();

    $sections.each(function() {
      var currentSection = $(this);
      var sectionTop = currentSection.offset().top;

      if (scrollPosition >= sectionTop - 132) {
        var id = currentSection.attr('id');
        var $navigationLink = sectionIdTonavigationLink[id].parent();

        var $viewport = $('.owl-stage-outer');
        var viewportWith = $viewport.width();
        var $owlItem = $navigationLink.parent();
        var owlItemIndex = $owlItem.index();
        var owlItemOffset = $owlItem.width() * owlItemIndex;
        var slide = Math.floor(owlItemOffset / viewportWith);

        letterIndexOwl.trigger('to.owl.carousel', [slide]);

        if ($navigationLink.hasClass('is-disabled')) return false;

        if (!$navigationLink.hasClass('is-active')) {

            $navigationLinks.parent().removeClass('is-active');

            $navigationLink.addClass('is-active');
        }
        return false;
      }

    });
}

function stickyLetterIndex() {
  var scrollTop = $(window).scrollTop();
  var offsetTop = $('.indice_content').offset().top;
  var $menuSticky = $('.menu_sticky');
  var $index = $('.letter-index_wrapper');
  // console.log($menuSticky.height(), scrollTop, offsetTop, offsetTop - scrollTop);
  if ($menuSticky.height() >= (offsetTop - scrollTop)) {
    if(!$index.hasClass('fixed')) {
      // letterIndexOwl.trigger('refresh.owl.carousel');
      $index.addClass('fixed');
    }
  } else {
    if($index.hasClass('fixed')) {
      // letterIndexOwl.trigger('refresh.owl.carousel');
      $index.removeClass('fixed')
    }
  }

}


var sizeIndexCarouse = function () {
  if( $(window).width() > 1199) return false;

  var $list = $('.marcas_indice-letras_lista');
  var $viewport = $list.parent();
  var $letters = $('.letra', $list);
  var $arrows = $('.marcas_indice-letras_arrow');

  console.log($letters);
}

var letterIndexOwl;
var enableIndexCarousel = function() {

  letterIndexOwl = $('.letter-index_letters').owlCarousel({
    margin:0,
    responsiveClass:true,
    // autoWidth:true,
    responsive:{
        0:{
            items:10,
            nav:true
        },
        700: {
          items:15,
          nav:false
        },
        900:{
            items:20,
            nav:false
        },
        1200:{
            items:27,
            nav:false,
            loop:false
        }
    }
  })


  $('.letter-index_arrow.is-right').on('click', function() {
    letterIndexOwl.trigger('next.owl.carousel');
    //letterIndexOwl.trigger('to.owl.carousel', [2]);
  })
  // Go to the previous item
  $('.letter-index_arrow.is-left').on('click', function() {
    letterIndexOwl.trigger('prev.owl.carousel');
  })

  // var $list = $('.marcas_indice-letras_lista');
  // var $viewport = $list.parent();

  // $('.marcas_indice-letras_arrow').on('click', function(e) {
  //   e.preventDefault();
  //   var $arrow = $(this)

  //   var transform = $list.css('transform');
  //   var cX = transform === 'none' ? 0 : parseInt(transform.split(',')[4]);

  //   var viewportWidth = $viewport.width() - 80;
  //   var listWith = $list.width();
  //   var minX = 0;
  //   var maxX = - (listWith - viewportWidth);
  //   var X = 0;

  //   if ($arrow.hasClass('is-right')) {
  //     X = Math.max(maxX, cX - viewportWidth)
  //   } else {
  //     X = Math.min(minX, cX + viewportWidth)
  //     console.log(X)
  //   }

  //   $list.css({'transform' : 'translate(' + X +'px, 0)'});
  // })

  // $(window).resize(function(){
  //   if( $(window).width() > 991) {
  //     $list.css({'transform' : 'translate(0, 0)'});
  //   }
  // })
}

$(window).resize(function(){
  stickyLetterIndex()
});

$(document).ready(function(){

  enableIndexCarousel();
  stickyLetterIndex();
  highlightNavigation();
  sizeIndexCarouse();

});

$(window).scroll(function() {

  highlightNavigation();
  stickyLetterIndex();

});




